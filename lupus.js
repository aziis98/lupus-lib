
export const UI = {};

UI.text = (text, options = {}) => {
    return { type: 'text', text, options };
};
UI.button = (text, options = {}) => {
    return { type: 'input.button', text, options };
};
UI.inputNumber = (initialValue, options = {}) => {
    return { type: 'input.number', value: initialValue, options };
};
UI.inputText = (initialValue, options = {}) => {
    return { type: 'input.text', value: initialValue, options };
};

export class Ruleset {
    constructor({ settings: { validate, render }, game }) {
        this.settingsFunctions = { validate, render };
        this.gameFunction = game;
    }

    validateSettings(settingsState) {
        return this.settingsFunctions.validate(settingsState);
    }

    updateSettings(settingsState) {
        return this.settingsFunctions.render(settingsState);
    }

    createGame(users, settingsState, handler) {
        const generator = this.gameFunction();
        // ...
    }
}

export class User {
    /**
     * @param {*} id string or numeric identifier of the user without any spaces
     * @param {*} name Nickname of the user, possibly without spaces
     * @param {*} data Other custom data owned by the user
     */
    constructor(id, name, data) {
        this.id = id;
        this.name = name;
        Object.assign(this, data);
    }

    toString() {
        return `@${this.id}`;
    }
}

