
const ACTIONS = [
    [
        {
            type: 'player.prompt-ok', 
            target: 'player-1',
            message: 'Il tuo ruolo è il contadino'
        },
        {
            type: 'player.prompt-ok', 
            target: 'player-2',
            message: 'Il tuo ruolo è il lupo'
        },
        {
            type: 'player.prompt-ok', 
            target: 'player-3',
            message: 'Il tuo ruolo è il veggente'
        },
    ],
    [
        {
            type: 'player.prompt-ok', 
            target: 'player-1',
            message: 'È giorno, iniziamo a votare?'
        },
        {
            type: 'player.prompt-ok', 
            target: 'player-2',
            message: 'È giorno, iniziamo a votare?'
        },
        {
            type: 'player.prompt-ok', 
            target: 'player-3',
            message: 'È giorno, iniziamo a votare?'
        },
    ],
    [
        {
            type: 'player.prompt-list', 
            target: 'player-1',
            message: 'Chi vuoi votare al rogo stasera?',
            choices: ['player-1', 'player-2', 'player-3']
        },
        {
            type: 'player.prompt-list', 
            target: 'player-2',
            message: 'Chi vuoi votare al rogo stasera?',
            choices: ['player-1', 'player-2', 'player-3']
        },
        {
            type: 'player.prompt-list', 
            target: 'player-3',
            message: 'Chi vuoi votare al rogo stasera?',
            choices: ['player-1', 'player-2', 'player-3']
        },
    ],
    [
        // TODO: ...
    ]
];

export default {
    settings: [
        ['numero-contadini', 'input.number', 0, { min: 0, max: 20 }],
        ['numero-lupi', 'input.number', 0, { min: 0, max: 5 }],
        ['numero-veggenti', 'input.number', 0, { min: 0, max: 5 }],
        ['numero-fattucchiere', 'input.number', 0, { min: 0, max: 10 }],
    ],
    onSettingsChanged(settings) {
        // Takes in the current map of settings values and returns a new settings control list
    },
    *game() {
        const [admin] = this.users.filter(user => user.admin);
        const players = this.users.filter(user => !user.admin);

        yield players.map(
            player => player.promptOk(`Il tuo ruolo è il ${player.ruolo}`)
        );



    }
}



