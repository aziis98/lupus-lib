import { defineRuleset } from "../../lupus.js";



export default class Ruleset1 extends Ruleset {
    constructor() {
        super();

        this.settings = [
            this.numeroContadini = new UI.Settings.Number('n° contadini', 3, { min: 0, max: 30 }),
            this.numeroLupi = new UI.Settings.Number('n° lupi', 2, { min: 0, max: 30 }),
            this.numeroVeggenti = new UI.Settings.Number('n° veggenti', 1, { min: 0, max: 30 }),
            // ...
        ];
    }
    
    onSettingChange(e) {
        console.log(this.numeroContadini.value);
    }
    
    onStart() {

    }
}

