
# lupus-lib

Questa è una libreria per descrivere in modo abbastanza generico i regolamenti delle partite di _lupus_ e di _lupus-live_. Per ora è altamente in fase prototipale.

## Usage

Essendo una libreria non c'è ancora veramente un utilizzo, per ora è principalmente uno strumento per verificare l'eventuale correttezza dei regolamenti già esistenti e scollegarli completamente da front-end e back-end.

Segue un esempio di utilizzo di un eventuale regolamento di esempio.

```js
import ExampleRuleset from  'example-lupus-ruleset';
import { User } from 'lupus-lib';

// Bind current ruleset for brevity
const ruleset = ExampleRuleset;

// Loop until creation settings are valid
let settingsState = {};
while (ruleset.validateSettings(settingsState)) {
    /* await */ UI.render(ruleset.getSettingsLayout(settingsState));
    settingsState = /* await */ UI.getState();
}

const users = [
    new User('user-0', 'Admin'),
    new User('user-1', 'Player1'),
    new User('user-2', 'Player2'),
    new User('user-3', 'Player3'),
    new User('user-4', 'Player4'),
    new User('user-5', 'Player5'),
];

const handler = (message, resolve) => {
    // ...  
};

const game = ruleset.createGame(users, settingsState, handler);
game.start();
```

### TODO: SaaS

[Coming Soon] Using [`lupus-service`](#todo-saas) you can easily deploy this library as a (stateless) service on an HTTP server.

```bash
git clone .../lupus-service
cd lupus-service
npm install
npm run start
```

## Rulesets

Adesso racconterò delle prime idee architettoniche riguardo la creazione dei regolamenti personalizzati.

### Creating a ruleset

L'unità di base è il `Ruleset` che definisce le impostazioni richieste per creare la partita ed il suo svolgimento.

### Widgets / Generic user interfaces

Per adesso non ho ancora trovato una soluzione migliore che non richieda qualcosa di questo tipo o macchinazioni strane di reflection quindi per ora ci sono delle funzioni.

(For now this idea makes me suffer a bit because this looks like yet another VDOM format)

```js
import { UI } from 'lupus-lib';
```

**Input fields:**

- `UI.inputNumber(initialValue, options?)`

    ```js
    { type: 'input.number', value: initialValue, options }
    ```

- `UI.inputText(initialValue, options?)`

    ```js
    { type: 'input.text', value: initialValue, options }
    ```

- `UI.inputChoice(initialKey, choiceMap, options?)`

    ```js
    { type: 'input.choice', value: initialKey, choiceMap, options }
    ```

- `UI.button(label, options)`

    ```js
    { type: 'input.button', label, options }
    ```

**Wrappers:**

- `UI.label(label, child, options?)`

    ```js
    { type: 'labelled', child, options }
    ```

- `UI.spoiler(label, child, options?)`

    ```js
    { type: 'spoiler', child, options }
    ```


**Labelled input fields:**

- `UI.number(label, initialValue, options?)`

    ```js
    { type: 'labelled', child: { type: 'input.number', ... } ... }
    ```

- `UI.text(label, initialValue, options?)`

    ```js
    { type: 'labelled', child: { type: 'input.text', ... } ... }
    ```

- `UI.choice(label, initialKey, choiceMap, options?)`

    ```js
    { type: 'labelled', child: { type: 'input.choice', ... } ... }
    ```

**Prompts:**

- `UI.promptOk(target, message, options?)`

    ```js
    { type: 'prompt.ok', target, message, options }
    ```

- `UI.promptText(target, message, options?)`

    ```js
    { type: 'prompt.text', target, message, options }
    ```

- `UI.promptChoice(target, message, choices, options?)`

    ```js
    { type: 'prompt.choice', target, message, choices, options }
    ```


**(?) Layout:**

Forse questi sono un po' troppo potenti/specifici e non verranno aggiunti nella versione definitiva. (e basterà usere le liste per concatenare i vari widget).

- `UI.row(children, options?)`

    ```js
    { type: 'row', children, options }
    ```

- `UI.column(children, options?)`

    ```js
    { type: 'column', children, options }
    ```

### Actually creating a ruleset

#### V1

This is the basic structure of a ruleset.

```js
import { Ruleset, UI } from 'lupus-lib';

export default new Ruleset({
    settings: {
        validate(settingsState) {
            return true;
        },
        render(settingsState) {
            return {
                numeroContadini:    UI.number(1, { min: 1, max: 30 }),
                numeroLupi:         UI.number(1, { min: 1, max: 30 }),
                numeroVeggenti:     UI.number(1, { min: 1, max: 30 }),
                numeroFattucchiere: UI.number(1, { min: 1, max: 30 }),
            };
        },
    },
    game: function* (users) {
        const [admin] = users.filter(user => user.admin);
        const players = users.filter(user => !user.admin);
        
        yield players.map(player => 
            UI.promptOk(player, 'È notte, chiudi gli occhi')
        );

        // ...
    }
})
```

#### V2

Questa è una versione alternativa senza coroutine con uno stile più comune, vagamente orientato ad oggetti.

```js
import { Ruleset, SettingsHandler, GameHandler, UI } from 'lupus-lib';

export default {
    // SettingsHandler dispatches `setup` and `validate`
    settings: class MySettings extends SettingsHandler {
        // @override
        setup() {
            this.ui.settings = [
                UI.number(1, { min: 1, max: 30, id: 'numeroContadini' }),
                UI.number(1, { min: 1, max: 30, id: 'numeroLupi' }),
                UI.number(1, { min: 1, max: 30, id: 'numeroVeggenti' }),
                UI.number(1, { min: 1, max: 30, id: 'numeroFattucchiere' }),
            ];
        }

        // @override
        validate(settingsState) {
            // ...
            // this.ui.settings = ...
            // ...
            return true;
        }
    },
    game: class MyGame extends GameHandler {
        // @override
        setup(settingsState, users) {        
            // `this.gameState` will be saved on the DB after `onStart`.
            // The rest of the game can be regenerated just from the action list.
            this.gameState = {
                // ...settingsState,
                // ...randomlyGeneratedParameters,
                admins: Object.fromEntries(
                    users.filter(user => user.role === 'admin').map(user => [user.id, user])
                ),
                players: Object.fromEntries(
                    users.filter(user => user.role !== 'admin').map(user => [user.id, user])
                ),
            };

            // ...randomly set `this.gameState.players[].role` based on settings...
        }

        // @override
        serialize() {
            return this.gameState;
        }

        // @override
        deserialize(o) {
            this.gameState = o;
        }

        // @override
        onAction(action) {
            // `onAction` should modify only `this.gameState` as internal state and for the rest be a pure function
            
            if (action.type == 'termina_notte') {
                this.gameState.giornata++;
                this.gameState.fase = 'alba';
                
                this.onAlba();
            }
            
            // ...

            if (action.type == 'player_submit') {
                const player = this.gameState.players[action.player];
                // ...
                if (player.role === 'lupo') {
                    player.giocatorePuntato = action.giocatorePuntato;
                }
                // ...
            }

            // ...
        }

        onAlba(action) {
            // Handler for phase "alba"...
        }

        onGiorno(action) {
            // Handler for phase "giorno"...

            for (const player of this.gameState.players) {
                // The ui is updated from the state of `this.ui` after every action call
                this.ui.users[player] = [
                    UI.text('È giorno, questa notte è morto...')
                ];
            }

            // ...
        }
        
        // ...
    },
}
```

Sample implementation of some of the classes

```js
// abstract class
class Handler {
    constructor() {
        // Default internal state variable
        this.state = { };
        // Used to modify remote UIs
        this.ui = { };
    }

    serialize() {
        return this.state;
    }
    
    deserialize(o) {
        this.state = o;
    }

    // abstract Handler.onEvent(e)
}

// abstract class
class SettingsHandler extends Handler {
    constructor() {
        super();

        // Just a convention to cleanly separate the UI in various sections
        this.ui = { settings: [] };
    }

    // Called by the library user to get settings UI
    getSettingsUI() {
        return this.ui.settings || [];
    }

    // @override
    onEvent(e) {
        if (e.type === 'setup') 
            return this.setup();
        if (e.type === 'validate') 
            return this.validate(e.data);
        else 
            // ...
    }
    
    // abstract SettingsHandler.setup()
    // abstract SettingsHandler.validate(settingsState)
}

// abstract class
class GameHandler extends Handler {
    constructor() {
        super();

        // Just a convention to cleanly separate the UI in various sections
        this.ui = { users: { } };
    }

    // Called by the library user to get current UI controls for a user
    getUserUI(user) {
        return this.ui.users[user] || [];
    }

    // @override
    onEvent(e) {
        if (e.type === 'setup') {
            e.users.forEach(user => this.ui.users[user] = []);
            return this.setup(e.settings, e.users);
        } else {
            return this.onAction(e);
        }
    }

    // abstract SettingsHandler.setup()
    // abstract SettingsHandler.onAction(e)
}
```

## Testing

For testing purposes the handler can be easily mocked with something like the following

```js
const PARTITA_DI_ESEMPIO = [
    [{ type: 'message-type-1', ... }, 'resolve-value-1'],
    [{ type: 'message-type-2', ... }, 'resolve-value-2'],
    [{ type: 'message-type-3', ... }, 'resolve-value-3'],
    [{ type: 'message-type-4', ... }, 'resolve-value-4'],
    // ...
    [   
        { 
            type: 'player.prompt', 
            target: 'user-1', 
            message: 'Chi vuoi uccidere?'
        },
        'user-2'
    ],
    // ...
];

// ...

let i = 0;
const handler = (resolve, data) => {
    const [_data, _resolve] = PARTITA_DI_ESEMPIO[i++];
    expect(data).toEqual(_data);
    resolve(_resolve);
}

/* await */ game.start([...], {...}, handler);

// ...

```




